class ChoiceListForm {
  constructor() {
    this.parentNode = document.querySelector('.sub-form');
    this.updateFields();
    this.updateList();
  }

  addButton(label) {
    let input = this.lastField.querySelector('input');
    let button = document.createElement('button');

    if (label == '+') {
      button.classList.add('add-choice');
    } else {
      button.classList.add('remove-choice');
    }

    if (input.classList.contains('input-error')) {
      button.classList.add('has-error');
    } else {
      button.classList.remove('has-error');
    }

    button.innerHTML = label;
    this.lastField.appendChild(button);

    return button;
  }

  removeButtons() {
    let buttons = this.lastField.querySelectorAll('button');
    buttons.forEach((button) => {
      button.remove();
    });
  }

  addField() {
    let field = document.createElement('p');
    field.classList.add('input');

    let label = document.createElement('label');
    label.htmlFor = 'choices';
    label.innerHTML = `Choice no. ${this.fieldsLength + 1}`;

    let input = document.createElement('input');
    input.type = 'text';
    input.name = 'choices[]';

    field.appendChild(label);
    field.appendChild(input);
    this.parentNode.appendChild(field);
    this.updateList();
  }

  removeField() {
    this.lastField.remove();
    this.updateList();
  }

  updateFields() {
    this.fields = this.parentNode.querySelectorAll('.input');
    this.fieldsLength = this.fields.length;
    this.lastField = this.fields[this.fieldsLength - 1];
  }

  updateList() {
    this.removeButtons();
    this.updateFields();

    let add = this.addButton('+');
    add.addEventListener('click', (ev) => {
      ev.preventDefault();
      console.log('click add');
      this.addField();
    });

    if (this.fieldsLength > 2) {
      let remove = this.addButton('-');
      remove.addEventListener('click', (ev) => {
        ev.preventDefault();
        console.log('click remove');
        this.removeField();
      });
    }
  }
}

document.addEventListener("DOMContentLoaded", () => {
    tail.DateTime(".datetime-field", {stayOpen: true});
    let list = new ChoiceListForm();
});

