<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $fillable = [
        'question_text', 'pub_date'
    ];

    public function wasPublishedRecently()
    {
        $yesterday = (new Carbon())->subDay();
        $pubDate = Carbon::parse($this->pub_date);
        return $pubDate->gte($yesterday) && $pubDate->lte(Carbon::now());
    }

    public function choices()
    {
        return $this->hasMany(Choice::class);
    }

    public function resolveRouteBinding($value, $field = null)
    {
        return $this->where([
            ['id', '=', $value],
            ['pub_date', '<=', Carbon::now()]
        ])->first();
    }
}
