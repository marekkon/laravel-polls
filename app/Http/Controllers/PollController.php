<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePollRequest;
use App\Models\Question;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PollController extends Controller
{
    public function index()
    {
        $questions = Question::where('pub_date', '<=', Carbon::now())
            ->orderBy('pub_date', 'desc')
            ->take(5)
            ->get();

        return view('polls.index', ['questions' => $questions]);
    }

    public function show(Question $question)
    {
        return view('polls.result', compact('question'));
    }

    public function vote(Question $question)
    {
        return view('polls.vote', compact('question'));
    }

    public function processVote(Question $question)
    {
        $choices = $question->choices->pluck('id');
        $attributes = request()->validate([
            'choice' => [
                'required',
                'integer',
                Rule::in($choices),
            ]
        ]);

        $choice = $question->choices->firstWhere('id', intval($attributes['choice']));
        $choice->votes += 1;
        $choice->save();

        return redirect()->route('result', $question);
    }

    public function create()
    {
        return view('polls.create');
    }

    public function store(StorePollRequest $request)
    {
        $question = Question::create([
            'question_text' => $request('question_text'),
            'pub_date' => $request('pub_date'),
        ]);

        $choices = [];
        foreach ($request('choices') as $choice) {
            $choices[] = ['choice_text' => $choice];
        }

        $question->choices()->createMany($choices);

        session()->forget('choices');
        return redirect()
            ->route('polls')
            ->with('flash', 'Poll has been created!');
    }
}
