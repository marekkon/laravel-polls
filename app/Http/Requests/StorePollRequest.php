<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class StorePollRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question_text' => 'required|max:200',
            'pub_date' => 'required',
            'choices.*' => 'max:200',
            'choices.0' => 'required',
            'choices.1' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'question_text.required' => 'The question is required.',
            'pub_date.required' => 'The publication date is required.',
            'choices.0.required' => 'The choice no. 1 is required.',
            'choices.1.required' => 'The choice no. 2 is required.',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator(Validator $validator)
    {
        $filledChoices = array_filter(
            $this->request->get('choices'), 
            fn($choice, $num) => $num < 2 || ($num >= 2 && $choice != null),
            ARRAY_FILTER_USE_BOTH
        );
        session(['choices' => count($filledChoices)]);
    }
}
