<?php

namespace Tests\Feature;

use App\Models\Question;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PollsIndexTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function no_questions()
    {
        $response = $this->get(route('polls'));

        $response->assertStatus(200);
        $response->assertSee('No polls are available.');
        $response->assertViewHas('questions', new Collection());
    }

    /** @test */
    public function past_questions()
    {
        $question = $this->createQuestion('Past question', -30);
        $response = $this->get(route('polls'));
        
        $response->assertViewHas(
            'questions',
            fn(Collection $users) => $users->contains($question)
        );
    }

    /** @test */
    public function future_questions()
    {
        $question = $this->createQuestion('Future question', 30);
        $response = $this->get(route('polls'));
        
        $response->assertSee('No polls are available.');
        $response->assertViewHas('questions', new Collection());
    }

    /** @test */
    public function future_question_and_past_question()
    {
        $pastQuestion = $this->createQuestion('Past question', -30);
        $futureQuestion = $this->createQuestion('Future question', -30);

        $response = $this->get(route('polls'));

        $response->assertViewHas(
            'questions',
            fn(Collection $users) => $users->count(1) && $users->contains($pastQuestion)
        );
    }

    /** @test */
    public function two_past_questions()
    {
        $question1 = $this->createQuestion('Past question 1', -30);
        $question2 = $this->createQuestion('Past question 2', -30);

        $response  = $this->get(route('polls'));

        $response->assertViewHas(
            'questions',
            fn(Collection $users) => $users->contains($question2) && $users->contains($question1)
        );
    }

    private function createQuestion($question, $days)
    {
        return Question::create([
            'question_text' => $question,
            'pub_date' => (new Carbon())->addDays($days),
        ]);
    }
}
