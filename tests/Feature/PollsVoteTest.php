<?php

namespace Tests\Feature;

use App\Models\Question;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PollsVoteTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function past_question()
    {
        $question = $this->createQuestion('Past question', -30);
        $response = $this->get(route('vote', $question->id));

        $response->assertSee($question->question_text);
    }

    /** @test */
    public function future_question()
    {
        $question = $this->createQuestion('Future question', 30);
        $response = $this->get(route('vote', $question->id));

        $response->assertStatus(404);
    }

    private function createQuestion($question, $days)
    {
        return Question::create([
            'question_text' => $question,
            'pub_date' => (new Carbon())->addDays($days),
        ]);
    }
}
