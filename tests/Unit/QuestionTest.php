<?php

namespace Tests\Unit;

use App\Models\Question;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class QuestionTest extends TestCase
{
    /** @test */
    public function published_recently_with_future_question()
    {
        $time = (new Carbon())->addDays(30);
        $futureQuestion = new Question();
        $futureQuestion->pub_date = $time;
        $this->assertFalse($futureQuestion->wasPublishedRecently());
    }

    /** @test */
    public function published_recently_with_old_question()
    {
        $time = (new Carbon())->subDay()->subSecond();
        $oldQuestion = new Question();
        $oldQuestion->pub_date = $time;
        $this->assertFalse($oldQuestion->wasPublishedRecently());
    }

    /** @test */
    public function published_recently_with_recent_question()
    {
        $time = (new Carbon())->subHours(23)->subMinutes(59)->subSeconds(59);
        $recentQuestion = new Question();
        $recentQuestion->pub_date = $time;
        $this->assertTrue($recentQuestion->wasPublishedRecently());
    }
}
