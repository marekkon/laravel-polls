<?php

use App\Http\Controllers\PollController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('polls');
});

Route::get('polls', [PollController::class, 'index'])->name('polls');

Route::get('polls/create', [PollController::class, 'create'])->name('create');

Route::post('polls', [PollController::class, 'store'])->name('store');

Route::get('polls/{question}', [PollController::class, 'show'])->name('result');

Route::get('polls/{question}/vote', [PollController::class, 'vote'])->name('vote');

Route::post('polls/{question}/vote', [PollController::class, 'processVote'])->name('vote_save');


