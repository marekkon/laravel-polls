@extends('_base')

@section('header')
  <header>
    <a href="{{ route('polls') }}">Back to index</a> 
  </header>
@endsection

@section('content')
  <h1>Poll results: {{ $question->question_text }}</h1>
  
  <ul>
  @foreach($question->choices as $choice)
    <li class="row"><span>{{ $choice->choice_text }}</span><span>{{ $choice->votes }}</span></li>
  @endforeach
  </ul>
@endsection
