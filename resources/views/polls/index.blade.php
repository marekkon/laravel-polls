@extends('_base')

@section('content')
  <h1>Polls</h1>
  <ul>
  @forelse($questions as $question)
    <li class="row">
      <a href="{{ route('vote', $question) }}">{{ $question->question_text }}</a>
      <a href="{{ route('result', $question) }}">Result</a>
    </li>
  @empty
    <p>No polls are available.</p>
  @endforelse
  </ul>
@endsection
