@extends('_base')

@section('header')
  <header>
    <a href="{{ route('polls') }}">Back to index</a> 
  </header>
@endsection

@section('content')
  <h1>Poll: {{ $question->question_text }}</h1>
  @error('choice')
  <h2>{{ $message }}</h2>
  @enderror
  
  <form action="{{ route('vote_save', $question) }}" method="post">
  @csrf
  @foreach($question->choices as $choice)
    <p>
      <input type="radio" name="choice" value="{{ $choice->id }}" id="choice-{{ $choice->id }}">
      <label for="choice-{{ $choice->id }}">{{ $choice->choice_text }}</label>
    </p>
  @endforeach
    <input type="submit" value="Vote">
  </form>
@endsection
