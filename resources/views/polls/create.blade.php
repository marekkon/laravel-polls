@extends('_base')

@section('scripts')
  <script defer src="/js/tail.datetime.js"></script>
  <script defer src="/js/form.js"></script>
  <link rel="stylesheet" href="/css/tail.datetime-default-blue.css">
@endsection

@section('header')
  <header>
    <a href="{{ route('polls') }}">Back to index</a> 
  </header>
@endsection

@section('content')
  <h1>Create poll</h1>
  @error('choice')
  <h2>{{ $message }}</h2>
  @enderror
  <form action="{{ route('store') }}" method="post">
  @csrf
    <p class="input">
      <label for="question_text">Question</label>
      <input
        type="text"
        name="question_text"
        class="@error('question_text') input-error @enderror"
        value="{{ old('question_text') }}"
      >
      @error('question_text')
      <span class="form-error">{{ $message }}<span>
      @enderror
    </p>
    <p class="input">
      <label for="pub_date">Publication date</label>
      <input
        type="datetime"
        name="pub_date"
        class="datetime-field @error('pub_date') input-error @enderror"
        value="{{ old('pub_date') }}"
      >
      @error('pub_date')
      <span class="form-error">{{ $message }}<span>
      @enderror
    </p>
    <div class="sub-form">
  @foreach(range(1, session('choices') ?? 2) as $choiceNumber)
      <p class="input">
        <label for="choices">Choice no. {{ $choiceNumber }}</label>
        @php $choiceFieldName = 'choices.'.($choiceNumber - 1) @endphp
        <input
          type="text"
          name="choices[]"
          class="@error($choiceFieldName) input-error @enderror"
          value="{{ old($choiceFieldName) }}"
        >
        @error($choiceFieldName)
        <span class="form-error">{{ $message }}<span>
        @enderror
      </p>
  @endforeach
    </div>
    <input type="submit" value="Save">
  </form>
@endsection
