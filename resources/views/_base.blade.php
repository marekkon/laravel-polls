<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Polls</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  @yield('scripts')
  <link href="https://fonts.googleapis.com/css2?family=Alegreya:wght@900&family=Roboto&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="/css/style.css">
</head>

<body>
  @yield('header')
  <div class="container">
    @yield('content')
  </div>

</body>

</html>
